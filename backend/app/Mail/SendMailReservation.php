<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendMailReservation extends Mailable
{
    use Queueable;
    use SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    private $userReservation;

    public function __construct(array $userReservation)
    {
        $this->userReservation = $userReservation;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject(
            "[Uqey] 予約リマインド (" .
                $this->userReservation["check_time"] .
                ")"
        )
            ->from(config("mail.from.address"), config("mail.from.name"))
            ->view("emails.user_reservation", [
                "user" => $this->userReservation,
            ]);
    }
}
