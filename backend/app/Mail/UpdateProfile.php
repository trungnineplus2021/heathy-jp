<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class UpdateProfile extends Mailable
{
    use Queueable;
    use SerializesModels;

    private $changeData;
    private $inputData;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($changeData, $inputData)
    {
        $this->changeData = $changeData;
        $this->inputData = $inputData;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject(
            __("[Uqey] アカウント情報変更完了お知らせ")
        )->view("emails.update_profile", [
            "data" => $this->changeData,
            "inputData" => $this->inputData,
        ]);
    }
}
