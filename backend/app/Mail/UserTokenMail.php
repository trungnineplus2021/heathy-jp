<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class UserTokenMail extends Mailable
{
    use Queueable;
    use SerializesModels;

    private $userTokenData;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($userTokenData)
    {
        $this->userTokenData = $userTokenData;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject(__("messages.EUA002_008"))->view(
            "emails.user_token",
            ["token" => $this->userTokenData["token"]]
        );
    }
}
