<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendMailUserEkyc extends Mailable
{
    use Queueable;
    use SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    private $userEkyc;
    private $url;

    public function __construct(array $userEkyc, $url)
    {
        $this->userEkyc = $userEkyc;
        $this->url = $url;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject("[Uqey] 免許証登録のリマインド")
            ->from(config("mail.from.address"), config("mail.from.name"))
            ->view("emails.user_ekyc", [
                "user" => $this->userEkyc,
                "url" => $this->url,
            ]);
    }
}
