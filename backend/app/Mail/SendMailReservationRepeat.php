<?php

namespace App\Mail;

use App\Enums\SendMailReservationsType;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendMailReservationRepeat extends Mailable
{
    use Queueable;
    use SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    private $dataSendMail;

    private $type;

    public function __construct(array $dataSendMail, string $type)
    {
        $this->dataSendMail = $dataSendMail;
        $this->type = $type;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $subject = "[Uqey] 返却延長通知";

        if ($this->type === SendMailReservationsType::BEFORE->value) {
            $subject = "[Uqey] 貸出前通知";
        } elseif ($this->type === SendMailReservationsType::AFTER->value) {
            $subject = "[Uqey] 返却前通知";
        }

        return $this->subject($subject)
            ->from(config("mail.from.address"), config("mail.from.name"))
            ->view("bookings.reservationRepeat", [
                "data" => $this->dataSendMail,
                "type" => $this->type,
            ]);
    }
}
