<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Mail\Mailables\Content;
use Illuminate\Mail\Mailables\Envelope;
use Illuminate\Queue\SerializesModels;

class UserReservationMailEkyc extends Mailable
{
    use Queueable;
    use SerializesModels;

    private $user;

    /**
     * Create a new message instance.
     *
     * @param $user
     *
     * @return void
     */
    public function __construct($user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject(__('[Uqey] 免許証登録のリマインド'))->view(
            'emails.email_user_ekyc',
            [
                'data' => $this->user,
            ]
        );
    }
}
