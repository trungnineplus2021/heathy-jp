<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class CancelReservationMail extends Mailable
{
    use Queueable;
    use SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    private $bookingData;
    private $name;

    public function __construct($bookingData, $name)
    {
        $this->bookingData = $bookingData;
        $this->name = $name;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject("[Uqey] 予約キャンセル完了通知")->view(
            "bookings.cancel",
            [
                "data" => $this->bookingData,
                "name" => $this->name,
            ]
        );
    }
}
