<?php

namespace App\Rules;

use App\Models\UserTokens;
use Illuminate\Contracts\Validation\Rule;

class UserTokenEmailExist implements Rule
{
    /**
     * @param mixed $message
     */
    protected $message;

    /**
     * Create a new rule instance.
     * @param mixed $message
     *
     * @return void
     */
    public function __construct($message)
    {
        $this->message = $message;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $email = strtolower($value);
        $userToken = UserTokens::where("email", cryptEncrypt($email))->first();

        return $userToken !== null;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->message;
    }
}
