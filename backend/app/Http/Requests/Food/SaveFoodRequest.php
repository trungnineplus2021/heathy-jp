<?php

namespace App\Http\Requests\Food;

use Illuminate\Foundation\Http\FormRequest;

class SaveFoodRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'food_name'         => [
                'required',
                'max:255'
            ],
            'image'             => [
                $this->isMethod('POST') ? 'required' : 'nullable',
                'file',
                'mimes:png,jpg,jpeg',
                'max:3072'
            ],
            'time'              => [
                'required',
                'date_format:H:i'
            ],
            'food_description'  => [
                'required',
            ],
            'calorie'           => [
                'required',
                'numeric'
            ],
            'fat'               => [
                'required',
                'numeric'
            ],
        ];
    }
}
