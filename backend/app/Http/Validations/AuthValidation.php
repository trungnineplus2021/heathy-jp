<?php

namespace App\Http\Validations;

use App\Enums\SocialType;
use App\Exceptions\BusinessException;
use App\Rules\UserEmailExist;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rules\Enum;

class AuthValidation
{
    /**
     * Validate login params request
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Validation\Validator|\Illuminate\Validation\Validator
     * @throws BusinessException
     */
    public function loginValidation(Request $request)
    {
        return Validator::make(
            $request->all(),
            [
                "email" => [
                    "bail",
                    "required",
                    "string",
                    "email",
                    "max:100",
                    new UserEmailExist("EUA001_005"),
                ],
                "password" => [
                    "bail",
                    "required",
                    "string",
                    // "min:8",
                    // 'regex:/^[a-zA-Z0-9]*([a-zA-Z][0-9]|[0-9][a-zA-Z])[a-zA-Z0-9]*$/',
                ],
            ],
            [
                "email.required" => "EUA001_001",
                "email.email" => "EUA001_002",
                "email.string" => "EUA001_002",
                "email.max" => "EUA001_002",
                "password.required" => "EUA001_003",
                // "password.*" => "EUA001_006",
            ]
        );
    }

    /**
     * Validate login by social params request
     *
     * @param Request $request
     * @return object
     */
    public function loginSocialValidation(Request $request)
    {
        return Validator::make(
            $request->all(),
            [
                "token" => "bail|required",
                "email" => "required",
                "type" => ["required", new Enum(SocialType::class)],
            ],
            [
                "token.*" => "EUA027_001",
                "email.*" => "EUA027_002",
                "type.required" => "EUA027_003",
                "type.*" => "EUA027_004",
            ]
        );
    }

    /**
     * Validate login param request
     *
     * @param  Request  $request
     * @return object
     */
    public function registerValidation(Request $request): object
    {
        return Validator::make(
            $request->all(),
            [
                "name" => "required|string|max:255",
                "email" => "required|string|email|max:255|unique:users",
                "password" => "required|string|min:6",
            ],
            [
                "name.*" => "Name error",
                "email.*" => "Email error",
                "password.*" => "password error",
            ]
        );
    }

    public function refreshValidation(Request $request): object
    {
        return Validator::make(
            $request->all(),
            [
                "email" => "required|email",
                "refresh_token" => "required",
            ],
            [
                "email.*" => "email error",
                "refresh_token.*" => "refresh_token error",
            ]
        );
    }
}
