<?php

namespace App\Http\Controllers;

use App\Http\Requests\Food\SaveFoodRequest;
use App\Services\FoodService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class FoodController extends Controller
{
    /**
     * @var FoodService
     */
    private $services;
    /**
     * Constructor
     *
     * @param FoodService $services
     */
    public function __construct(FoodService $services)
    {
        $this->services = $services;
    }

    /**
     *  Display a listing of the resource..
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(): JsonResponse
    {
        // Call function list at food service 
        $result = $this->services->list();
        return $this->successResponse('', $result);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  App\Http\Requests\Food\SaveFoodRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(SaveFoodRequest $request): JsonResponse
    {
        //Call function create at food service 
        $result = $this->services->create($request->validated());
        return $this->successResponse(__('messages.S001'), $result);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id): JsonResponse
    {
        // Call function find at food service 
        $result = $this->services->find($id);
        // If result is null then return not found
        if (is_null($result)) {
            return $this->errorResponse(__('messages.E001'), Response::HTTP_NOT_FOUND);
        }
        return $this->successResponse('', $result);
    }

    /**
     *  Update the specified resource in storage.
     *
     * @param  int  $id
     * @param  App\Http\Requests\Food\SaveFoodRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update($id, SaveFoodRequest $request): JsonResponse
    {
        // Call function update at food service 
        $result = $this->services->update($id, $request->validated());
        // If result is null then return not found
        if (is_null($result)) {
            return $this->errorResponse(__('messages.E001'), Response::HTTP_NOT_FOUND);
        }
        return $this->successResponse(__('messages.S002'), $result);
    }

    /**
     *  Delete the specified resource in storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id): JsonResponse
    {
        // Call function delete at food service 
        $this->services->delete($id);
        return $this->successResponse(__('messages.S003'));
    }
}
