<?php

namespace App\Http\Controllers;

use App\Http\Requests\FoodLog\SaveFoodLogRequest;
use App\Services\FoodLogService;
use App\Services\FoodService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class FoodLogController extends Controller
{
    /**
     * @var FoodService
     */
    private $services;
    /**
     * Constructor
     *
     * @param FoodLogService $services
     */
    public function __construct(FoodLogService $services)
    {
        $this->services = $services;
    }

    /**
     *  Display a listing of the resource..
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(): JsonResponse
    {
        // Call function list at food log service 
        $result = $this->services->list();
        return $this->successResponse('', $result);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  App\Http\Requests\Food\SaveFoodLogRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(SaveFoodLogRequest $request): JsonResponse
    {
        //Call function create at food log service 
        $result = $this->services->create($request->validated());
        return $this->successResponse(__('messages.S001'), $result);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id): JsonResponse
    {
        // Call function find at food log service 
        $result = $this->services->find($id);
        // If result is null then return not found
        if (is_null($result)) {
            return $this->errorResponse(__('messages.E001'), Response::HTTP_NOT_FOUND);
        }
        return $this->successResponse('', $result);
    }

    /**
     *  Delete the specified resource in storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id): JsonResponse
    {
        // Call function delete at food log service 
        $this->services->delete($id);
        return $this->successResponse(__('messages.S003'));
    }
}
