<?php

namespace App\Http\Controllers;

use App\Exceptions\BusinessException;
use App\Http\Validations\AuthValidation;
use App\Services\UserService;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use JWTAuth;
use App\Enums\HttpStatus;
use Illuminate\Support\Facades\Hash;
use App\Enums\UserStatus;
use Aws\CognitoIdentityProvider\Exception\CognitoIdentityProviderException;
use Ellaisys\Cognito\AwsCognito;
use Ellaisys\Cognito\AwsCognitoClaim;
use Ellaisys\Cognito\AwsCognitoClient;
use Illuminate\Support\Facades\Log;

class AuthController extends Controller
{
    protected AuthValidation $validation;
    protected UserService $userService;
    protected $cognito;

    public function __construct(
        AuthValidation $validation,
        UserService $userService,
        AwsCognito $cognito
    ) {
        $this->validation = $validation;
        $this->userService = $userService;
        $this->cognito = $cognito;
    }

    /**
     * Login.
     *
     * @param Request $request
     *
     * @return JsonResponse
     * @throws BusinessException
     */
    public function login(Request $request): JsonResponse
    {
        try {
            // $credentials = $request->only("email", "password");
            $validator = $this->validation->loginValidation($request);
            if ($validator->fails()) {
                throw new BusinessException($validator->errors()->first());
            }
            $data = $this->userService->loginCognito($request);
            return $this->successResponse($data);
        } catch (BusinessException $e) {
            throw $e;
        } catch (Exception $e) {
            throw new BusinessException("EUA000", previous: $e);
        }
    }

    /**
     * loginSocial.
     *
     * @param Request $request
     *
     * @return JsonResponse
     * @throws BusinessException
     */
    public function checkSnsToken(Request $request): JsonResponse
    {
        try {
            $validator = $this->validation->loginSocialValidation($request);
            if ($validator->fails()) {
                throw new BusinessException($validator->errors()->first());
            }

            if (!$request->hasHeader("udid")) {
                throw new BusinessException("EUA027_006");
            }
            $checkData = $this->userService->checkExitSocialUser(
                $request->email,
                $request->token,
                $request->type
            );
            if (!$checkData) {
                throw new BusinessException("EUA027_005");
            } else {
                $data = $this->userService->loginSocialAccount(
                    $request,
                    $checkData
                );
                return $this->successResponse($data);
            }
        } catch (BusinessException $e) {
            throw $e;
        } catch (Exception $e) {
            throw new BusinessException("E000", previous: $e);
        }
    }

    /**
     * register.
     *
     * @param Request $request
     *
     * @return object
     */
    public function register(Request $request)
    {
        try {
            $validator = $this->validation->registerValidation($request);
            if ($validator->fails()) {
                return $this->errorResponse($validator->errors());
            }
            $user = $this->userService->createUser(
                $request->name,
                $request->email,
                $request->password
            );
            $token = JWTAuth::fromUser($user);
            $data = [
                "user" => $user,
                "authorisation" => [
                    "token" => $token,
                    "type" => "bearer",
                ],
            ];
            return $this->successResponse($data, code: HttpStatus::CREATED);
        } catch (\Exception $e) {
            return $this->errorResponse($e->getMessage());
        }
    }

    /**
     * logout.
     *
     * @return object
     */
    public function logout(Request $request)
    {
        $dataRemoveDivece = $this->userService->removeDevice(
            $request->header("udid")
        );
        auth()->logout();
        return $this->successResponse(null, "Successfully logged out");
    }

    /**
     * Refresh a token.
     *
     * @return object
     */
    public function refreshToken(Request $request)
    {
        $validator = $this->validation->refreshValidation($request);
        if ($validator->fails()) {
            return $this->errorResponse($validator->errors());
        }

        try {
            $client = app()->make(AwsCognitoClient::class);
            $email = strtolower($request->email);
            //Get User Data
            $user = $client->getUser($email);

            //Use username from AWS to refresh token, not email from login!
            if (!empty($user["Username"])) {
                $response = $client->refreshToken(
                    $user["Username"],
                    $request->refresh_token
                );
                $user = $this->userService->getUserByEmail($email);
                $claim = new AwsCognitoClaim($response, $user, "email");
                $this->cognito->setClaim($claim)->storeToken();
                return $this->successResponse($claim);
            } else {
                return $this->errorResponse("Invalid user.");
            } //End if
        } catch (CognitoIdentityProviderException $exception) {
            Log::error("COGNITO-ERROR:" . $exception->getMessage());
            return $this->errorResponse("Invalid refresh token.");
        }
    }

    protected function createNewToken($token)
    {
        $data = [
            "access_token" => $token,
            "token_type" => "bearer",
            //            'expires_in' => auth()->factory()->getTTL() * 60,
            "user" => auth()->user(),
        ];
        return $this->successResponse($data);
    }
}
