<?php

namespace App\Http\Controllers;

use App\Services\ChartWeightService;
use Illuminate\Http\JsonResponse;

class ChartWeightController extends Controller
{
    /**
     * @var ChartWeightService
     */
    private $services;
    /**
     * Constructor
     *
     * @param ChartWeightService $services
     */
    public function __construct(ChartWeightService $services)
    {
        $this->services = $services;
    }

    /**
     *  Display a listing of the resource..
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(): JsonResponse
    {
        // Call function list at chart weight service 
        $result = $this->services->getBodyProportionByMonth();
        return $this->successResponse('', $result);
    }
}
