<?php

namespace App\Http\Controllers;

use App\Enums\HttpStatus;
use App\Enums\UserStatus;
use App\Enums\ConfirmVerifySmsStatus;
use App\Exceptions\BusinessException;
use App\Services\BookingService;
use App\Services\OperationService;
use App\Services\SubDriversService;
use App\Services\UserService;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Repositories\Interfaces\UserRepositoryInterface;
use App\Repositories\Interfaces\ReservationRepositoryInterface;
use App\Http\Validations\UserValidation;
use Illuminate\Support\Facades\DB;
use Laravel\Socialite\Facades\Socialite;
use App\Services\Gmo\Exceptions\GmoException;
use App\Traits\CheckStatusUser;
use Illuminate\Support\Facades\Log;
use Ellaisys\Cognito\Auth\ResetsPasswords as CognitoResetsPasswords;
use Carbon\Carbon;

class UserController extends Controller
{
    use CheckStatusUser;
    use CognitoResetsPasswords;

    private UserRepositoryInterface $userRepo;
    private UserService $userService;
    private UserValidation $userValidation;
    private BookingService $bookingService;
    private SubDriversService $subDriversService;
    private OperationService $operationService;
    public ReservationRepositoryInterface $reservationRepository;

    public function __construct(
        UserRepositoryInterface $userRepo,
        UserService $userService,
        UserValidation $userValidation,
        BookingService $bookingService,
        SubDriversService $subDriversService,
        OperationService $operationService,
        ReservationRepositoryInterface $reservationRepository
    ) {
        $this->userRepo = $userRepo;
        $this->userService = $userService;
        $this->userValidation = $userValidation;
        $this->bookingService = $bookingService;
        $this->subDriversService = $subDriversService;
        $this->operationService = $operationService;
        $this->reservationRepository = $reservationRepository;
    }
    //
    // public function index(Request $request)
    // {
    //     dd($request->all());
    // }

    /**
     * Get the authenticated User.
     *
     * @return JsonResponse
     */
    public function userProfile()
    {
        return $this->successResponse(auth()->user());
    }

    /**
     * @throws \Twilio\Exceptions\ConfigurationException
     */
    public function sendCode()
    {
        $user = $this->userRepo->first();
        $this->userService->sendCode($user->phone, "");
        return $this->successResponse($user);
    }

    public function updateProfile(Request $request)
    {
        try {
            $validator = $this->userValidation->updateProfileValidation(
                $request
            );
            if ($validator->fails()) {
                return $this->errorResponse($validator->errors());
            }
            $user = $this->userService->updateProfileUser(
                strtolower($request->email),
                $request->phone,
                $request->password
            );
            return $this->successResponse(null, "Successfully update");
        } catch (\Exception $e) {
            return $this->errorResponse($e->getMessage());
        }
    }

    // public function redirectToGoogle()
    // {
    //     return Socialite::driver("google")
    //         ->stateless()
    //         ->redirect();
    // }

    // /**
    //  * @throws BusinessException
    //  */
    // public function handleGoogleCallback()
    // {
    //     try {
    //         $user = Socialite::driver("google")
    //             ->stateless()
    //             ->user();
    //         if (empty($user)) {
    //             throw new Exception();
    //         }
    //         $res = $this->userService->loginWithGoogle($user);
    //         return $this->successResponse($res, code: HttpStatus::CREATED);
    //     } catch (Exception $e) {
    //         throw new BusinessException("E000", previous: $e);
    //     }
    // }

    /**
     * Send Mail code User.
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function sendMailCode(Request $request)
    {
        try {
            $validator = $this->userValidation->emailVerifyValidation($request);
            if ($validator->fails()) {
                throw new BusinessException($validator->errors()->first());
            }
            $response = $this->userService->saveUsersToken(
                strtolower($request->email)
            );
            if ($response === true) {
                return $this->successResponse(null);
            }
            return $this->errorResponse($response);
        } catch (BusinessException $e) {
            throw $e;
        } catch (Exception $e) {
            throw new BusinessException("E000", previous: $e);
        }
    }

    /**
     * Verify Email Code User.
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function verifyEmailCode(Request $request)
    {
        try {
            $validator = $this->userValidation->verifyEmailCodeValidation(
                $request
            );
            if ($validator->fails()) {
                throw new BusinessException($validator->errors()->first());
            }
            $response = $this->userService->createAccountUsers($request);
            return $this->successResponse($response, code: HttpStatus::CREATED);
        } catch (BusinessException $e) {
            throw $e;
        } catch (Exception $e) {
            throw new BusinessException("E000", previous: $e);
        }
    }

    public function getStatusEkycById(Request $request)
    {
        try {
            $data = $this->userService->getEkycStausById();
            return $this->successResponse([
                "ekyc_status" => $data->ekyc_status,
            ]);
        } catch (Exception $e) {
            throw new BusinessException("E000", previous: $e);
        }
    }

    /**
     * Send SMS code to phone user
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function sendSmsCode(Request $request)
    {
        try {
            $validator = $this->userValidation->smsVerifyValidation($request);
            if ($validator->fails()) {
                throw new BusinessException($validator->errors()->first());
            }
            $response = $this->userService->saveUserPhone($request->phone);
            if ($response === true) {
                return $this->successResponse(null);
            }
            return $this->errorResponse($response);
        } catch (BusinessException $e) {
            throw $e;
        } catch (Exception $e) {
            throw new BusinessException("E000", previous: $e);
        }
    }

    /**
     * Verify SMS Code to phone User.
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function verifySmsCode(Request $request)
    {
        try {
            if (!$request->has("type")) {
                throw new BusinessException("EUA005_009");
            }
            if (ConfirmVerifySmsStatus::CREATE_USER->equals($request->type)) {
                $validator = $this->userValidation->verifySmsCodeCreateUserValidation(
                    $request
                );
                if ($validator->fails()) {
                    throw new BusinessException($validator->errors()->first());
                }
            }
            if (ConfirmVerifySmsStatus::UPDATE_PHONE->equals($request->type)) {
                $validator = $this->userValidation->verifySmsCodeUpdatePhoneValidation(
                    $request
                );
                if ($validator->fails()) {
                    throw new BusinessException($validator->errors()->first());
                }
            }
            $this->userService->updateAccountUsers($request);
            return $this->successResponse(null);
        } catch (BusinessException $e) {
            throw $e;
        } catch (Exception $e) {
            throw new BusinessException("E000", previous: $e);
        }
    }

    public function creditInfo()
    {
        try {
            $this->checkStatusUser();
            $data = $this->userService->creditInfo();
            return $this->successResponse($data);
        } catch (BusinessException $e) {
            throw $e;
        } catch (GmoException $e) {
            throw $e;
        } catch (Exception $e) {
            throw new BusinessException("E000", previous: $e);
        }
    }

    /**
     * UA-025_Withdrawal
     * @throws BusinessException
     */
    public function withdrawal(Request $request)
    {
        try {
            if (!$request->has("withdrawal_reason")) {
                throw new BusinessException("EUA025_002");
            }
            $withdrawalReason = json_decode($request->withdrawal_reason, true);
            if (empty($withdrawalReason)) {
                throw new BusinessException("EUA025_002");
            }
            $this->checkStatusUser();
            $notPaidReservation = $this->bookingService->getNotPaidReservation();
            if (!$notPaidReservation->isEmpty()) {
                throw new BusinessException("EUA025_001");
            }
            $this->userService->updateUserStatus(
                UserStatus::WITHDRAWAL,
                $request
            );
            auth("api")->logout();
            return $this->successResponse(null);
        } catch (BusinessException $e) {
            throw $e;
        } catch (Exception $e) {
            throw new BusinessException("EUA000", previous: $e);
        } finally {
            // $request->session()->invalidate();
        }
    }

    public function getUserInfo(Request $request)
    {
        try {
            $this->checkStatusUser();
            $data = $this->userService->getUserInfo($request);
            if (!$data) {
                throw new BusinessException("EUA000");
            }
            return $this->successResponse($data);
        } catch (BusinessException $e) {
            throw $e;
        } catch (Exception $e) {
            throw new BusinessException("E000", previous: $e);
        }
    }

    public function changeAccountInfo(Request $request)
    {
        try {
            $this->checkStatusUser();
            $validator = $this->userValidation->changeInfoValidation($request);
            if ($validator->fails()) {
                throw new BusinessException($validator->errors()->first());
            }
            $data = $this->userService->updateUserInfo($request);
            if (!$data) {
                throw new BusinessException("EUA000");
            }
            return $this->successResponse(null);
        } catch (BusinessException $e) {
            throw $e;
        } catch (Exception $e) {
            throw new BusinessException("E000", previous: $e);
        }
    }

    public function changePassword(Request $request)
    {
        try {
            $this->checkStatusUser();
            $validator = $this->userValidation->changePassWordValidation(
                $request
            );
            if ($validator->fails()) {
                throw new BusinessException($validator->errors()->first());
            }
            $this->userService->changePassWordCog(
                $request->old_password,
                $request->new_password
            );
            return $this->successResponse(null);
        } catch (BusinessException $e) {
            throw $e;
        } catch (Exception $e) {
            throw new BusinessException("EUA000", previous: $e);
        }
    }

    public function updateEKYCResult(Request $request)
    {
        try {
            $this->checkStatusUser();
            $validator = $this->userValidation->updateEKYCResult($request);
            if ($validator->fails()) {
                throw new BusinessException($validator->errors()->first());
            }
            $this->userService->updateEKYCResult(
                $request->ekyc_id,
                $request->licence_id,
                $request->valid_date,
                $request->status,
                $request->address,
                $request->name,
                $request->birthday,
                $request->issue_date,
                $request->issuing_organization,
                $request->license_conditions,
                $request->issue_date1,
                $request->issue_date2,
                $request->issue_date3,
                $request->good_driver,
                $request->license_type,
                $request->license_color,
                $request->remarks
            );
            return $this->successResponse(null);
        } catch (BusinessException $e) {
            throw $e;
        } catch (Exception $e) {
            throw new BusinessException("EUA000", previous: $e);
        }
    }

    public function addCreditCard(Request $request)
    {
        try {
            $this->checkStatusUser();
            $validator = $this->userValidation->addCreditCardValidation(
                $request
            );
            if ($validator->fails()) {
                throw new BusinessException($validator->errors()->first());
            }
            $this->userService->addCreditCard($request->brand, $request->token);
            return $this->successResponse(null, "カード追加を完了しました。");
        } catch (BusinessException $e) {
            throw $e;
        } catch (GmoException $e) {
            throw $e;
        } catch (Exception $e) {
            throw new BusinessException("EUA000", previous: $e);
        }
    }

    public function resetPasswordSendcode(Request $request)
    {
        try {
            $validator = $this->userValidation->resetPasswordCodeValidation(
                $request
            );
            if ($validator->fails()) {
                throw new BusinessException($validator->errors()->first());
            }
            $this->userService->resetPasswordSendcode(
                strtolower($request->email)
            );
            return $this->successResponse(null);
        } catch (BusinessException $e) {
            throw $e;
        } catch (Exception $e) {
            throw new BusinessException("EUA000", previous: $e);
        }
    }

    public function resetPassword(Request $request)
    {
        try {
            $validator = $this->userValidation->resetPasswordValidation(
                $request
            );
            if ($validator->fails()) {
                throw new BusinessException($validator->errors()->first());
            }
            $request->request->add([
                "password_confirmation" => $request->confirm_password,
            ]);
            $request->request->add(["token" => $request->email_verify_code]);
            // $this->reset($request);
            try {
                $result = $this->reset($request);
            } catch (Exception $e) {
                Log::error("COGNITO-ERROR:" . $e->getMessage());
                throw new BusinessException("EUA000", previous: $e);
            }
            $message = json_decode($result->getContent())?->message;
            if ($message == "This password reset token is invalid.") {
                throw new BusinessException("EUA031_010");
            }
            $this->userService->resetPassword($request);
            return $this->successResponse(null);
        } catch (BusinessException $e) {
            throw $e;
        } catch (Exception $e) {
            throw new BusinessException("EUA000", previous: $e);
        }
    }

    public function deletecredit(Request $request)
    {
        try {
            $this->checkStatusUser();
            if (!$request->has("user_payment_id")) {
                throw new BusinessException("EUA032_001");
            }
            $this->userService->deletecredit($request->user_payment_id);
            return $this->successResponse(null);
        } catch (BusinessException $e) {
            throw $e;
        } catch (GmoException $e) {
            throw $e;
        } catch (Exception $e) {
            throw new BusinessException("EUA000", previous: $e);
        }
    }

    public function getFreeKey(Request $request)
    {
        try {
            $data = $this->userService->getFreeKey($request);
            return $this->successResponse($data);
        } catch (Exception $e) {
            throw new BusinessException("EUA000", previous: $e);
        }
    }

    public function checkEkycValid()
    {
        try {
            $data = $this->userService->checkEkycValid();
            return $this->successResponse($data);
        } catch (Exception $e) {
            throw new BusinessException("EUA000", previous: $e);
        }
    }

    public function removeDevice(Request $request)
    {
        try {
            if (!$request->hasHeader("udid")) {
                throw new BusinessException("EUA036_001");
            }
            $data = $this->userService->removeDevice($request->header("udid"));
            return $this->successResponse($data);
        } catch (Exception $e) {
            throw new BusinessException("E000", previous: $e);
        }
    }

    public function updateBiometric()
    {
        try {
            $this->userRepo->updateBiometric();
            return $this->successResponse(null);
        } catch (Exception $e) {
            throw new BusinessException("E000", previous: $e);
        }
    }

    /**
     * UA-041
     *
     * @param Request $request
     * @return JsonResponse
     * @throws BusinessException
     */
    public function updateSubDriver(Request $request): JsonResponse
    {
        DB::beginTransaction();
        try {
            $params = [
                "sub_driver_email_list" => $request["sub_driver_email_list"],
                "reservation_id" => $request["reservation_id"],
            ];

            // required reservation id
            if (empty($request["reservation_id"])) {
                throw new BusinessException("EUA041_005");
            }

            $check = $this->reservationRepository->find(
                $request["reservation_id"]
            );

            if (Carbon::parse($check["end_time"]) < Carbon::now()) {
                throw new BusinessException("EUA041_007");
            }

            // 1. Check car has started
            $reservationStart = $this->operationService->getOperationsByReservationId(
                $params["reservation_id"]
            );
            if ($reservationStart) {
                return $this->errorResponse(
                    __("messages.EUA041_006"),
                    "EUA041_006"
                );
            }

            if (gettype($params["sub_driver_email_list"]) === "string") {
                $params["sub_driver_email_list"] = json_decode(
                    $params["sub_driver_email_list"],
                    true
                );
            }

            // 2. Delete all sub driver by reservation id if list email is empty
            if (!$params["sub_driver_email_list"]) {
                $this->subDriversService->deleteSubDriversByReservationId(
                    $params["reservation_id"]
                );
                DB::commit();
                return $this->successResponse(null, __("messages.SUA041_001"));
            }

            // 3. Validation
            $this->userValidation->listEmailValidation($params);

            // 4. Handle validation input list email have in db and update
            $result = $this->userService->updateSubDriver($params);

            DB::commit();
            if (!$result["status"]) {
                return $this->errorResponse($result["msg"]);
            }
            return $this->successResponse(null, $result["msg"]);
        } catch (BusinessException $e) {
            throw $e;
        } catch (Exception $e) {
            DB::rollBack();
            throw new BusinessException("E000", previous: $e);
        }
    }

    public function updateGender(Request $request)
    {
        try {
            $validator = $this->userValidation->updateValidationGender(
                $request
            );
            if ($validator->fails()) {
                throw new BusinessException($validator->errors()->first());
            }
            $data = $this->userService->updateGender($request);
            if (!$data) {
                throw new BusinessException("EUA000");
            }
            return $this->successResponse(null);
        } catch (BusinessException $e) {
            throw $e;
        } catch (Exception $e) {
            throw new BusinessException("E000", previous: $e);
        }
    }

    public function saveLogEkyc(Request $request)
    {
        try {
            if (!$request->has("response")) {
                throw new BusinessException("EUA046_001");
            }
            if (!$request->has("type")) {
                throw new BusinessException("EUA046_002");
            }
            $response = json_encode($request->response, JSON_UNESCAPED_UNICODE);
            Log::info($response);
            if ($request->type == 0) {
                Log::error("EKYC-ERROR:" . $request->messages);
            }
            return $this->successResponse(null);
        } catch (BusinessException $e) {
            throw $e;
        } catch (Exception $e) {
            throw new BusinessException("E000", previous: $e);
        }
    }

    public function getDrawalReason()
    {
        try {
            $data = $this->userService->getDrawalReason();
            return $this->successResponse($data);
        } catch (Exception $e) {
            throw new BusinessException("EUA000", previous: $e);
        }
    }
}
