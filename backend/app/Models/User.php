<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

/**
 * App\Models\User
 */
class User extends Authenticatable
{
    use HasApiTokens;
    use HasFactory;
    use Notifiable;

    protected $fillable = [
        "first_name",
        "last_name",
        "first_name_kana",
        "last_name_kana",
        "email",
        "password",
        "remember_token",
        "birthday",
        "address1",
        "status",
        "google_id",
        "apple_id",
        "ekyc_status",
        "biometric_cancel",
        "reg_step",
        "freekey_user_id",
        "last_login",
        "gender",
        "count_wrong_pwd",
        "province",
        "user_withdrawal_at",
        "withdrawal_reason",
        "withdrawal_reason_explain",
    ];

    protected $guarded = [];
    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = ["password"];

    /**
     * The attributes that should be visible for serialization.
     *
     * @var array<int, string>
     */
    protected $visible = [];

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
    public $timestamps = true;
}
