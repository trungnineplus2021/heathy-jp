<?php

namespace App\Traits;

use Illuminate\Http\Response;

trait ApiResponser
{
    /**
     * @param string $message
     * @param int $code
     * @param array $data
     * @return \Illuminate\Http\JsonResponse
     */
    protected function successResponse(
        $message = null,
        $data    = null,
        $code    = Response::HTTP_OK
    ) {
        return response()->json(
            [
                "status"    => "Success",
                "message"   => $message,
                "data"      => $data,
            ],
            $code
        );
    }

    /**
     * @param string $message
     * @param int $code
     * @param string $errorCode
     * @param array $data
     * @return \Illuminate\Http\JsonResponse
     */
    protected function errorResponse(
        $message    = null,
        $code       = Response::HTTP_BAD_REQUEST,
        $errorCode  = null,
        $data       = null
    ) {
        return response()->json(
            [
                "status"    => "Error",
                "message"   => $message,
                "code"      => $errorCode,
                "data"      => $data,
            ],
            $code
        );
    }
}
