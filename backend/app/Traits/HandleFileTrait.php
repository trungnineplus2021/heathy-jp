<?php

namespace App\Traits;

use Illuminate\Support\Facades\Storage;

trait HandleFileTrait
{
    /**
     * Upload file image and random name file
     *
     * @param mixed $file
     * @return string
     */
    private function uploadImage($file, $path)
    {
        // Get the uploaded file from the request
        $image = $file;
        // Set name file with uniqid
        $fileName = uniqid() . '.' . $image->getClientOriginalExtension();
        // Save the image to the storage disk with path is images
        Storage::disk('public')->putFileAs($path, $image, $fileName);

        return $fileName;
    }

    /**
     * Get url image
     *
     * @param string $fileName
     * @return string|null
     */
    private function getUrlImageByFileName($fileName)
    {
        // check the image exists in storage or not
        if (Storage::disk('public')->exists($fileName)) {
            // Get the URL of the image from the local disk
            return Storage::disk('public')->url($fileName);
        }
        return null;
    }

    /**
     * Delete image by file name
     *
     * @param string $fileName
     * @return bool|null
     */
    private function deleteImageByFileName($fileName)
    {
        // check the image exists in storage or not
        if (Storage::disk('public')->exists($fileName)) {
            // Get the URL of the image from the local disk
            return Storage::disk('public')->delete($fileName);
        }
        return null;
    }
}
