<?php

namespace App\Traits;

use App\Exceptions\BusinessException;

trait CheckStatusUser
{
    public function checkStatusUser()
    {
        if (object_get(auth()->user(), "status") == 2) {
            // auth()->logout();
            // session()->flush();
            return throw new BusinessException("EUA999_999");
        }
        return true;
    }
}
