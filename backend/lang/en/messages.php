<?php

return [
    // Message success
    'S001'      => 'Successful object create',
    'S002'      => 'Successful object update',
    'S003'      => 'Successful object delete',
    // Message error
    'E001'      => 'Can not find the object'
];
