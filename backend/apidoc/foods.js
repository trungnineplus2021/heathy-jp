/**
 * @api {get} /foods
 * @apiName List data food
 * @apiGroup Foods
 *
 * @apiSuccessExample {json} Success-Response
HTTP/1.1 200 Success
{
    "status": "Success",
    "message": "",
    "data": {
        "current_page": 1,
        "data": [
            {
                "id": 6,
                "food_name": "123",
                "image": "http://localhost:8081/storage/images/foods/641293120bfe1.jpg",
                "time": "11:11:00",
                "food_description": "123",
                "calorie": 43,
                "fat": 24
            },
            {
                "id": 8,
                "food_name": "com chien",
                "image": "http://localhost:8081/storage/images/foods/64128e5307488.jpg",
                "time": "13:15:00",
                "food_description": "oke",
                "calorie": 11,
                "fat": 23
            },
            {
                "id": 9,
                "food_name": "com chien 1",
                "image": "http://localhost:8081/storage/images/foods/641292ca73c46.jpg",
                "time": "13:15:00",
                "food_description": "oke",
                "calorie": 11,
                "fat": 23
            },
            {
                "id": 10,
                "food_name": "com chien 12",
                "image": "http://localhost:8081/storage/images/foods/641292d877749.jpg",
                "time": "13:15:00",
                "food_description": "oke",
                "calorie": 11,
                "fat": 23
            },
            {
                "id": 11,
                "food_name": "123",
                "image": "http://localhost:8081/storage/images/foods/6412a138460fa.jpg",
                "time": "11:11:00",
                "food_description": "123",
                "calorie": 43,
                "fat": 24
            }
        ],
        "first_page_url": "http://localhost:8081/api/foods?page=1",
        "from": 1,
        "last_page": 1,
        "last_page_url": "http://localhost:8081/api/foods?page=1",
        "links": [
            {
                "url": null,
                "label": "&laquo; Previous",
                "active": false
            },
            {
                "url": "http://localhost:8081/api/foods?page=1",
                "label": "1",
                "active": true
            },
            {
                "url": null,
                "label": "Next &raquo;",
                "active": false
            }
        ],
        "next_page_url": null,
        "path": "http://localhost:8081/api/foods",
        "per_page": 20,
        "prev_page_url": null,
        "to": 5,
        "total": 5
    }
}
 */

/**
 * @api {post} /foods
 * @apiName Create Food
 * @apiGroup Foods
 *
 * @apiParam  {String} [food_name] Name of food.
 * @apiParam  {String} [time] Meal time.
 * @apiParam  {String} [food_description] Detail of food.
 * @apiParam  {float} [calorie] Calorie.
 * @apiParam  {float} [fat] Fat.
 *
 * @apiSampleRequest  /api/foods
 *
 *
 * @apiSuccessExample {json} Success-Response
 */

/**
 * @api {get} /foods/{id}
 * @apiName Edit Food
 * @apiGroup Foods
 *
 * @apiSampleRequest  /api/foods/1
 *
 * @apiSuccessExample {json} Success-Response
 * HTTP/1.1 200 Success
{
    "status": "Success",
    "message": "",
    "data": {
        "id": 11,
        "food_name": "123",
        "image": "http://localhost:8081/storage/images/foods/6412a138460fa.jpg",
        "time": "11:11:00",
        "food_description": "123",
        "calorie": 43,
        "fat": 24,
        "created_at": "2023-03-16T04:54:55.000000Z",
        "updated_at": "2023-03-16T04:55:20.000000Z",
        "deleted_at": null
    }
}
 */

/**
 * @api {put} /foods/{id}
 * @apiName Edit Food
 * @apiGroup Foods
 *
 * @apiParam  {String} [food_name] Name of food.
 * @apiParam  {String} [time] Meal time.
 * @apiParam  {String} [food_description] Detail of food.
 * @apiParam  {float} [calorie] Calorie.
 * @apiParam  {float} [fat] Fat.
 *
 * @apiSampleRequest  /api/foods/1
 *
 * @apiSuccessExample {json} Success-Response
 */