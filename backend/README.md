### Installation Laravel ###
* `cp .env.example .env`
* `docker-compose up -d`
* `docker-compose exec php php composer install --ignore-platform-reqs`
* `docker-compose exec php php artisan migrate`
* `docker-compose exec php php artisan db:seed`

### Build api doc ###
* cd api-doc
* npm install apidoc -g
* apidoc -i . -o ../public/apidoc

### API Document ###

## Get Information Foods

```http
GET /api/foods/?page=1
```
## Responses

Json Response

```javascript
{
    "status": "Success",
    "message": "",
    "data": {
        "current_page": 1,
        "data": [
            {
                "id": 1,
                "food_name": "123",
                "image": "http://localhost:8081/storage/images/foods/6412a138460fa.jpg",
                "time": "11:11:00",
                "food_description": "123",
                "calorie": 43,
                "fat": 24
            }
        ],
        "first_page_url": "http://localhost:8081/api/foods?page=1",
        "from": 1,
        "last_page": 1,
        "last_page_url": "http://localhost:8081/api/foods?page=1",
        "links": [
            {
                "url": null,
                "label": "&laquo; Previous",
                "active": false
            },
            {
                "url": "http://localhost:8081/api/foods?page=1",
                "label": "1",
                "active": true
            },
            {
                "url": null,
                "label": "Next &raquo;",
                "active": false
            }
        ],
        "next_page_url": null,
        "path": "http://localhost:8081/api/foods",
        "per_page": 20,
        "prev_page_url": null,
        "to": 1,
        "total": 1
    }
}
```

## Create Information Foods

```http
POST /api/foods/?page=1
```

## Edit Information Foods

```http
PUT /api/foods/?page=1
```

## Show Information Foods

```http
GET /api/foods/?page=1
```

## Delete Information Foods

```http
Delete /api/foods/?page=1
```

## Get list history log foods

```http
Get /api/food-logs
```

## Show detail history log foods

```http
Get /api/food-logs/{id}
```

## Save history log foods

```http
POST /api/food-logs
```

## Delete history log foods

```http
Delete /api/food-logs/{id}
```

## Delete history log foods

```http
Delete /api/food-logs/{id}
```

## weight/fat percentage chart by month

```http
Get /api/weights
```