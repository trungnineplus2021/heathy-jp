{{ $inputData['last_name'] }} {{ $inputData['first_name'] }}様<br />
<br />
アカウント情報の変更が行われましたのでお知らせいたします。<br />
万が一、変更にお心当たりが無い場合不正アクセスの疑いがございますので、<br />
ご利用内容の確認や、パスワードの変更をご検討ください。<br />
<br />
------------------------------------------------------------------------------------<br />
【変更前】<br />
@if (array_key_exists('last_name', $data))
{{ "姓：".$data['last_name'] }}<br />
@endif
@if (array_key_exists('last_name_kana', $data))
{{ "姓（フリガナ）：".$data['last_name_kana'] }}<br />
@endif
@if (array_key_exists('first_name', $data))
{{ "名：".$data['first_name'] }}<br />
@endif
@if (array_key_exists('first_name_kana', $data))
{{ "名（フリガナ）：".$data['first_name_kana'] }}<br />
@endif
@if (array_key_exists('address1', $data) || array_key_exists('province', $data))
{{ "住所：".$data['province'] ?? null.$data['address1'] ?? null }}<br />
@endif
@if (array_key_exists('birthday', $data))
{{ "生年月日：".\Carbon\Carbon::parse($data["birthday"])->format("Y/m/d") }}<br />
@endif
【変更後】<br />
@if (array_key_exists('last_name', $data))
{{ "姓：".$inputData['last_name'] }}<br />
@endif
@if (array_key_exists('last_name_kana', $data))
{{ "姓（フリガナ）：".$inputData['last_name_kana'] }}<br />
@endif
@if (array_key_exists('first_name', $data))
{{ "名：".$inputData['first_name'] }}<br />
@endif
@if (array_key_exists('first_name_kana', $data))
{{ "名（フリガナ）：".$inputData['first_name_kana'] }}<br />
@endif
@if (array_key_exists('address1', $data) || array_key_exists('province', $data))
{{ "住所：".$inputData['province'] ?? null.$inputData['address1'] ?? null }}<br />
@endif
@if (array_key_exists('birthday', $data))
{{ "生年月日：".\Carbon\Carbon::parse($inputData["birthday"])->format("Y/m/d") }}<br />
@endif
------------------------------------------------------------------------------------<br />
<br />
このEメールアドレスは配信専用です。<br />
返信は受付できませんのでご了承ください。<br />
