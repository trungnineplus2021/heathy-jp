{{ $user['last_name'] }} {{ $user['first_name'] }} 様<br />
<br />
Uqeyをご利用頂きまして、ありがとうございます。<br />
以下の予約が近づいてきたため、お知らせいたします。<br />
<br />
【予約内容】<br />
予約管理番号：{{ $user['booking_id'] }}<br />
出発ステーション：{{ $user['start_station'] }}<br />
返却ステーション：{{ $user['end_station'] }}<br />
車両：{{ $user['brand'] . ' ' . $user['name'] }}<br />
車両ナンバー：{{ $user['vehicle_number'] }}<br />
予約日時：{{ \Carbon\Carbon::parse($user['start_time'])->format('Y/m/d H:i') }}<br />
返却日時：{{ \Carbon\Carbon::parse($user['end_time'])->format('Y/m/d H:i') }}<br />
予約時間：{{ $user['time_distance'] }}<br />
予約時間料金：{{ number_format($user['usage_fee'], 0, ',') }} 円<br />
@if (!empty($user['accessories']))
    オプション：<br />
    @foreach ($user['accessories'] as $item)
        {{ $item['name'] }} x{{ $item['quantity'] }}：{{ number_format($item['total_fee_actual'], 0, ',') }}円<br />
    @endforeach

@endif
@if (!empty($user['insurances']))
    保険：<br />
    @foreach ($user['insurances'] as $item)
        @if ($item['calc_day'] != 0)
        {{ $item['name'] }} x{{ $item['calc_day'] }}日分：{{ number_format($item['total_fee_actual'],0,',') }}円<br />
        @else
        {{ $item['name'] }}：{{ number_format($item['total_fee_actual'],0,',') }}円<br />
        @endif
    @endforeach
@endif
<br />
料金合計：{{ number_format($user['total_amount']) }}円<br />
<br />
運転者情報：{{$user['nameSub']}}<br />
<br />
キャンセルポリシー：<br />
@if ($user['max_amount'] || $user['max_amount'] === 0)
キャンセル料金 上限 {{ number_format($user['max_amount'], 0, ',') }}円<br />
@else
キャンセル料金 上限 無し<br />
@endif
@if (!empty($user['cancel_plan']))
    @foreach ($user['cancel_plan'] as $item)
        {{ $item['day_number'] }}日前 料金の{{ $item['rate']}}％<br />
    @endforeach
@endif
<br />
ご利用には、運転免許証のご登録が必要です。<br />
まだご登録がお済みでない場合は、ご利用日までに<br />
Uqeyスマートフォンアプリからご登録をお願いいたします。<br />
{{$user['url_download_app']}}<br />
{{ $user['reminder_email_add_message'] }}<br />
<br />
----------------------------------------------------------------------------<br />
【お問い合わせ先】<br />
{{ $user['franchise_name'] }} {{ $user['start_station'] }}<br />
電話番号：{{ $user['start_phone'] }}<br />
@if ($user['always_open'])
営業時間：24時間営業<br />
@else
営業時間：{{ \Carbon\Carbon::parse($user['start_business_time'])->format('H:i') }}～{{ \Carbon\Carbon::parse($user['end_business_time'])->format('H:i') }}<br />
@endif
住所：{{ $user['start_address'] }}<br />
----------------------------------------------------------------------------<br />
<br />
このEメールアドレスは配信専用です。<br />
返信は受付できませんのでご了承ください。<br />

