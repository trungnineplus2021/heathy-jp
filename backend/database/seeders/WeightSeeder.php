<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;

use App\Models\Weight;
use Illuminate\Database\Seeder;

class WeightSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $data = [];
        for ($i = 1; $i <= 30; $i++) {
            $data[] = [
                'date'      => '2023-03-' . (($i < 10) ? '0' . $i : $i),
                'weight'    => random_int(60, 80),
                'user_id'   => 1
            ];
        }
        Weight::insert($data);
    }
}
