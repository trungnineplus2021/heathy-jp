FROM composer:latest

WORKDIR /var/www/html

ENTRYPOINT [ "composer" ]
# ENTRYPOINT [ "composer", "--ignore-platform-reqs" ]